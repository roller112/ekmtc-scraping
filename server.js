let rp = require('request-promise');
let cheerio = require('cheerio');
let tough = require('tough-cookie');
let moment = require('moment');
const readline = require('readline');

let searchKeyword = 'KMTCCCU0002879';

const r1 = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const webUrl = 'http://www.ekmtc.com/CCIT100/searchContainerList.do';
const googleAPI = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json';
const googleDetailAPI = 'https://maps.googleapis.com/maps/api/place/details/json';
const googleKEY = 'AIzaSyDJXD_kg9-QouZDczKsxDogDh9zjVvKdHU';


let getAddressFromPort = async (portName) => {
  const options2 = {
    method: 'GET',
    uri: googleAPI,
    qs: {
      input: portName,
      inputtype: 'textquery',
      key: googleKEY
    },
    headers: {
      'User-Agent': 'Request-Promise',
    },
    json: true
  };
  
  const placeIDRes = await rp(options2);

  if (placeIDRes.candidates.length === 0) {
    const options1 = {
      method: 'GET',
      uri: 'https://geocoder.api.here.com/6.2/geocode.json',
      // jar: cookiejar,
      qs: {
        app_id: '3Wt8KnudGvHpJyttt9Ue',
        app_code: 'SVqP5rpzTzy7sn6m4Fe5AQ',
        searchtext: portName
      },
      headers: {
          'User-Agent': 'Request-Promise'
      },
      json: true
    };

    const temp = await rp(options1);
    if (temp.Response && temp.Response.View && temp.Response.View.length > 0 && temp.Response.View[0].Result
      && temp.Response.View[0].Result.length > 0 && temp.Response.View[0].Result[0].Location.Address) {
      return {
        country: temp.Response.View[0].Result[0].Location.Address.Country || '',
        region: temp.Response.View[0].Result[0].Location.Address.County || '',
        city: temp.Response.View[0].Result[0].Location.Address.City || '',
      }
    }
    return {};
  }
  const options3 = {
    method: 'GET',
    uri: googleDetailAPI,
    qs: {
      placeid: placeIDRes.candidates[0].place_id,
      fields: 'address_components,formatted_address,name',
      key: googleKEY
    },
    headers: {
      'User-Agent': 'Request-Promise',
    },
    json: true
  };
  const placeDetail = await rp(options3);
  return getFormattedAddress(placeDetail.result.address_components);
};

let getFormattedAddress = (addressArr) => {
  let country = '';
  let region = '';
  let city = '';
  for (let i = 0; i < addressArr.length; i++) {
    if (addressArr[i].types.includes('country')) {
      country = addressArr[i].short_name;
    }
    if (addressArr[i].types.includes('administrative_area_level_1')) {
      region = addressArr[i].short_name;
    }
    if (addressArr[i].types.includes('locality')) {
      city = addressArr[i].short_name;
    }
  }
  return {
    country,
    region,
    city
  };
}

let scrape = async () => {

  const options = {
    method: 'POST',
    uri: webUrl,
    formData: {
      dt_knd: 'BL',
      own_yn: 'N',
      flag: 'O',
      condition: 'BL',
      bl_no: searchKeyword
    },
    transform: function (body) {
      return cheerio.load(body);
    }
  };

  const $ = await rp(options);

  $.prototype.textln = function () {
    this.find('br').replaceWith('\n');
    this.find('*').each(function () {
      $(this).replaceWith($(this).html());
    });
    return this.html();
  }
  const rows = $('.Board_row2').find('tr');
  let output = {};
  let flow = [];
  for (let i = 1; i < rows.length; i++) {
    const current = rows[i];
    const cells = $(current).find('td');
    if (cells.length > 1 && i === 1) {
      output.bl = `KMTC${$(cells[0]).text()}`;
    }
    const tempBegin = i === 1 ? 3 : 0;
    for (let j = tempBegin; j < cells.length; j=j + 4) {
      let temp1 = $(cells[j]).html();
      temp1 = temp1.split('<br>');

      let temp2 = $(cells[j+1]).html();
      temp2 = temp2.split('<br>');

      let temp3 = $(cells[j+2]).html();
      temp3 = temp3.split('/');

      let temp4 = $(cells[j+3]).text();

      const fromPort = await getAddressFromPort(temp1[0]);
      const toPort = await getAddressFromPort(temp2[0]);

      flow.push({
        from: {
          port: fromPort,
          date: temp1[1]
        },
        to: {
          port: toPort,
          date: temp2[1]
        },
        vessel: temp3[0].split(')')[1],
        voyage: temp3[1],
        status: temp4
      });
    }
  }

  let destination = {};
  let currentLocation = {};
  let originalLocation = {};
  let status = 'IN-DELIVERY';
  let eta = '';
  let currentVessel = '';
  let currentVoyage = '';

  for (let i = 0; i < flow.length; i ++ ){
    const fromPort = flow[i].from.port;
    const fromDate = moment(flow[i].from.date, 'YYYY.MM.DD/HH:mm');
    const toPort = flow[i].to.port;
    const toDate = moment(flow[i].to.date, 'YYYY.MM.DD/HH:mm');
    const cDate = new Date();

    if (i === 0) {
      originalLocation = fromPort;
    }

    if (i === flow.length - 1) {
      eta = moment(flow[i].to.date, 'YYYY.MM.DD/HH:mm');
      destination = toPort;
      if (cDate >= toDate) {
        status = 'COMPLETE';
      }
    }

    if (cDate >= fromDate) {
      if (cDate < toDate) {
        currentLocation = fromPort;
        currentVessel = flow[i].vessel;
        currentVoyage = flow[i].voyage;
      } else if (i === flow.length - 1) {
        currentLocation = toPort;
        currentVessel = flow[i].vessel;
        currentVoyage = flow[i].voyage;
      }
    } else {
      if (i > 0) {
        currentLocation = flow[i-1].to.port;
        currentVessel = flow[i-1].vessel;
        currentVoyage = flow[i-1].voyage;
      }
    }
  }

  return {
    bl: output.bl,
    vesselName: currentVessel,
    voygaeNumber: currentVoyage,
    eta: moment(eta).format(moment.HTML5_FMT.DATETIME_LOCAL_MS),
    status,
    locations: [
      originalLocation,
      currentLocation,
      destination
    ]
  };
};

r1.question('Input tracking number, container number or BOL number:', (input) => {
  console.log('[---Searching for ---]', input);
  searchKeyword = input;

  if (input) {
    console.log('[---Start scrapping----]');
    scrape().then(value => {
      console.log('[---value---]', value);
    });
  }

  r1.close();
})


