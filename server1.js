let rp = require('request-promise');
let cheerio = require('cheerio');
let tough = require('tough-cookie');
let moment = require('moment');

const webUrl = 'http://www.ekmtc.com/CCIT100/searchContainerList.do';
const googleAPI = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json';
const googleDetailAPI = 'https://maps.googleapis.com/maps/api/place/details/json';
const googleKEY = 'AIzaSyDJXD_kg9-QouZDczKsxDogDh9zjVvKdHU';


let getAddressFromPort = async (portName) => {
  const options2 = {
    method: 'GET',
    uri: googleAPI,
    qs: {
      input: portName,
      inputtype: 'textquery',
      key: googleKEY
    },
    headers: {
      'User-Agent': 'Request-Promise',
    },
    json: true
  };
  
  const placeIDRes = await rp(options2);

  if (placeIDRes.candidates.length === 0) {
    return {};
  }
  const options3 = {
    method: 'GET',
    uri: googleDetailAPI,
    qs: {
      placeid: placeIDRes.candidates[0].place_id,
      fields: 'address_components,formatted_address,name',
      key: googleKEY
    },
    headers: {
      'User-Agent': 'Request-Promise',
    },
    json: true
  };
  const placeDetail = rp(options3)
  // return getFormattedAddress(placeDetail.result.address_components);
  .then(response => {
    console.log('[---response---]', response);
    const options3 = {
      method: 'GET',
      uri: googleDetailAPI,
      qs: {
        placeid: response.candidates[0].place_id,
        fields: 'address_components,formatted_address,name',
        key: googleKEY
      },
      headers: {
        'User-Agent': 'Request-Promise',
      },
      json: true
    };
  
    rp(options3)
    .then(res => {
      console.log('[---detail---]', res.result.address_components);
      return getFormattedAddress(res.result.address_components);
    })
    .catch(e => {
      console.log('[---error---]', e);
      return e;
    })
  })
  .catch(err => {
    console.log('[---err---]', err);
    return err;
  });

  return returnValue;
};

getAddressFromPort('PORT KLANG WESTPORT').then(value => {
  console.log('[--value---]', value);
})

var algoliasearch = require('algoliasearch');
var places = algoliasearch.initPlaces('plVEQIL2Z6OG', '11d4e266bcb71074da22e7acf3260682');
places
  .search('BUSAN NEW PORT', function(err, res) {
    if (err) {
      throw err;
    }
    console.log('[---country---]',res.hits[0].country);
    console.log('[---city---]',res.hits[0].city);
    console.log('[---locale---]',res.hits[0].locale_names);
    console.log('[---highlight---]',res.hits[0]._highlightResult);
  });

let cookie = new tough.Cookie({
  key: 'Lang',
  value: 'ENG',
  domain: 'http://www.ekmtc.com',
  httpOnly: true,
  maxAge: 31536000
});

let cookiejar = rp.jar();
cookiejar.setCookie(cookie, 'http://www.ekmtc.com');

const options1 = {
  method: 'GET',
  uri: 'https://geocoder.api.here.com/6.2/geocode.json',
  // jar: cookiejar,
  qs: {
    app_id: '3Wt8KnudGvHpJyttt9Ue',
    app_code: 'SVqP5rpzTzy7sn6m4Fe5AQ',
    searchtext: 'PORT KLANG WESTPORT'
  },
  headers: {
      'User-Agent': 'Request-Promise'
  },
  json: true
};

const options = {
  method: 'POST',
  uri: webUrl,
  // jar: cookiejar,
  formData: {
    dt_knd: 'BL',
    own_yn: 'N',
    flag: 'O',
    condition: 'BL',
    bl_no: 'KMTCCCU0002879'
  },
  transform: function (body) {
    return cheerio.load(body);
  }
};

let getFormattedAddress = (addressArr) => {
  let country = '';
  let regision = '';
  let city = '';
  for (let i = 0; i < addressArr.length; i++) {
    if (addressArr[i].types.includes('country')) {
      country = addressArr[i].short_name;
    }
    if (addressArr[i].types.includes('administrative_area_level_1')) {
      region = addressArr[i].short_name;
    }
    if (addressArr[i].types.includes('locality')) {
      city = addressArr[i].short_name;
    }
  }
  return {
    country,
    region,
    city
  };
}

let getAddressFromPort = async (portName) => {
  const options2 = {
    method: 'GET',
    uri: googleAPI,
    qs: {
      input: portName,
      inputtype: 'textquery',
      key: googleKEY
    },
    headers: {
      'User-Agent': 'Request-Promise',
    },
    json: true
  };
  
  
  const returnValue = await rp(options2)
  .then(response => {
    console.log('[---response---]', response);
    const options3 = {
      method: 'GET',
      uri: googleDetailAPI,
      qs: {
        placeid: response.candidates[0].place_id,
        fields: 'address_components,formatted_address,name',
        key: googleKEY
      },
      headers: {
        'User-Agent': 'Request-Promise',
      },
      json: true
    };
  
    rp(options3)
    .then(res => {
      console.log('[---detail---]', res.result.address_components);
      return getFormattedAddress(res.result.address_components);
    })
    .catch(e => {
      console.log('[---error---]', e);
      return e;
    })
  })
  .catch(err => {
    console.log('[---err---]', err);
    return err;
  });

  return returnValue;
};

let scrape = async () => {
  const $ = await rp(options);

  $.prototype.textln = function () {
    this.find('br').replaceWith('\n');
    this.find('*').each(function () {
      $(this).replaceWith($(this).html());
    });
    return this.html();
  }
  const mainTable = $('.Board_row2');
  const rows = $('.Board_row2').find('tr');
  let output = {};
  let flow = [];
  for (let i = 1; i < rows.length; i++) {
    const current = rows[i];
    const cells = $(current).find('td');
    if (cells.length > 1 && i === 0) {
      output.bl = $(cells[0]).text();
    }
    const tempBegin = i === 1 ? 3 : 0;
    for (let j = tempBegin; j < cells.length; j=j + 4) {
      let temp1 = $(cells[j]).html();
      temp1 = temp1.split('<br>');

      let temp2 = $(cells[j+1]).html();
      temp2 = temp2.split('<br>');

      let temp3 = $(cells[j+2]).html();
      temp3 = temp3.split(' ');

      let temp4 = $(cells[j+3]).text();

      const fromPort = await getAddressFromPort(temp1[0]);
      const toPort = await getAddressFromPort(temp2[0]);

      flow.push({
        from: {
          port: fromPort,
          date: temp1[1]
        },
        to: {
          port: toPort,
          date: temp2[1]
        },
        vessel: temp3[0].split(')')[1],
        voyage: temp3[1],
        status: temp4
      });
    }
  }
  console.log('[---flow---]', flow);

  // .then($ => {
  //   $.prototype.textln = function () {
  //     this.find('br').replaceWith('\n');
  //     this.find('*').each(function () {
  //       $(this).replaceWith($(this).html());
  //     });
  //     return this.html();
  //   }
  //   const mainTable = $('.Board_row2');
  //   const rows = $('.Board_row2').find('tr');
  //   let output = {};
  //   let flow = [];
  //   for (let i = 1; i < rows.length; i++) {
  //     const current = rows[i];
  //     const cells = $(current).find('td');
  //     if (cells.length > 1 && i === 0) {
  //       output.bl = $(cells[0]).text();
  //     }
  //     const tempBegin = i === 1 ? 3 : 0;
  //     for (let j = tempBegin; j < cells.length; j=j + 4) {
  //       let temp1 = $(cells[j]).html();
  //       temp1 = temp1.split('<br>');
  
  //       let temp2 = $(cells[j+1]).html();
  //       temp2 = temp2.split('<br>');
  
  //       let temp3 = $(cells[j+2]).html();
  //       temp3 = temp3.split(' ');
  
  //       let temp4 = $(cells[j+3]).text();

  //       const fromPort = getAddressFromPort(temp1[0]);
  //       const toPort = getAddressFromPort(temp2[0]);
  
  //       flow.push({
  //         from: {
  //           port: fromPort,
  //           date: temp1[1]
  //         },
  //         to: {
  //           port: toPort,
  //           date: temp2[1]
  //         },
  //         vessel: temp3[0].split(')')[1],
  //         voyage: temp3[1],
  //         status: temp4
  //       });
  //     }
  //   }
  //   console.log('[---flow---]', flow);
  // })
  // .catch(err => {
  //   console.log('[---Error---]', err);
  // })
};

scrape().then(value => {
  console.log('[---value---]', value);
});
